<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="food and drinks" tilewidth="128" tileheight="96" tilecount="19" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="64" height="64" source="07_right_mate_bottles_flora.png"/>
 </tile>
 <tile id="1">
  <image width="32" height="32" source="01_mate.png"/>
 </tile>
 <tile id="2">
  <image width="32" height="96" source="02_mate_crates_1.png"/>
 </tile>
 <tile id="3">
  <image width="32" height="96" source="03_mate_crates_2.png"/>
 </tile>
 <tile id="4">
  <image width="64" height="64" source="05_mate_bottles_full.png"/>
 </tile>
 <tile id="5">
  <image width="64" height="64" source="06_mate_bottles.png"/>
 </tile>
 <tile id="6">
  <image width="64" height="64" source="08_right_mate_bottles_flora_full.png"/>
 </tile>
 <tile id="7">
  <image width="32" height="32" source="10_coffee.png"/>
 </tile>
 <tile id="8">
  <image width="32" height="32" source="11_coffee2.png"/>
 </tile>
 <tile id="9">
  <image width="32" height="32" source="12_Donuts.png"/>
 </tile>
 <tile id="10">
  <image width="64" height="64" source="15_Pizza.png"/>
 </tile>
 <tile id="11">
  <image width="32" height="32" source="16_Pizza_klein.png"/>
 </tile>
 <tile id="12">
  <image width="32" height="32" source="17_kaffetasse1.png"/>
 </tile>
 <tile id="13">
  <image width="32" height="32" source="18_tablett.png"/>
 </tile>
 <tile id="14">
  <image width="64" height="64" source="19_getraenkeautomat.png"/>
 </tile>
 <tile id="15">
  <image width="128" height="64" source="20_premium_crate.png"/>
 </tile>
 <tile id="16">
  <image width="128" height="64" source="21_premium_crate2.png"/>
 </tile>
 <tile id="17">
  <image width="128" height="64" source="22_flora_crate.png"/>
 </tile>
 <tile id="18">
  <image width="32" height="32" source="23_Plätzchen.png"/>
 </tile>
</tileset>
